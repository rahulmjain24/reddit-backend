const { Pool } = require('pg')
const { password, user, database, host } = require('../config/config')

const pool = new Pool({
    user: user,
    password: password,
    host: host,
    database: database
})

// Get all the posts
const getSeries = (req, res) => {
    pool.query('select * from reddit_data order by id asc', (err, data) => {
        if (err) {
            console.error(err)
            res.json({ error: err })
        } else {
            res.json(data.rows)
        }
    })
}

// Get single post by it's ID
const getById = (req, res) => {
    pool.query(`select * from reddit_data where id=${req.params.id}`, (err, data) => {
        if (err) {
            console.error(err)
            res.json({ error: err })
        } else {
            if(data.rows.length === 0) {
                res.json({ error: 'Post not found'})
            } else {
                res.json(data.rows[0])
            }
        }
    })
}

//Create a new post
const createNewPost = (req, res) => {
    const newPostData = req.body
    console.log(newPostData)
    const query = `insert into reddit_data (title, post, sub_reddit, user_name, votes, comments, image, r_image) values (
        '${newPostData.title.replace(/'/ig,"''")}', 
        '${newPostData.post.replace(/'/ig,"''")}', 
        '${newPostData.sub_reddit.replace(/'/ig,"''")}', 
        '${newPostData.user_name.replace(/'/ig,"''")}', 
        '${newPostData.votes}', 
        '${newPostData.comments}',
        '${newPostData.image.replace(/'/ig,"''")}',
        '${newPostData.r_image.replace(/'/ig,"''")}'
    )`
    console.log(query)
    pool.query(query, (err, data) => {
        if (err) {
            console.error(err)
            res.json({ error: err })
        } else {
            res.send('created')
        }
    })
}

// Update posts by ID
const updateById = (req, res) => {
    console.log(req)
    const dataToBeUpdated = req.body
    pool.query(`select * from reddit_data where id=${req.params.id}`, (err, data) => {
        if (err) {
            console.error(err)
            res.json({ error: err })
        } else {
            if(data.rows.length === 0) {
                res.json({ error: 'Post not found'})
            } else {
                const originalData = data.rows[0]
                const query = `update reddit_data set title = '${dataToBeUpdated.title !== undefined ? dataToBeUpdated.title.replace(/'/ig, `''`) : originalData.title.replace(/'/ig, `''`)}', post = '${dataToBeUpdated.post !== undefined ? dataToBeUpdated.post.replace(/'/ig, `''`) : originalData.post.replace(/'/ig, `''`)}', image = '${dataToBeUpdated.image !== undefined ? dataToBeUpdated.image.replace(/'/ig, `''`) : originalData.image.replace(/'/ig, `''`)}', votes = '${dataToBeUpdated.votes !== undefined ? dataToBeUpdated.votes : originalData.votes}' where id = ${req.params.id}`
                pool.query(query, (err, updated) => {
                    if (err) {
                        console.error(err)
                        res.json({ error: err })
                    } else {
                        res.json({message: 'Update success'})
                    }
                })
            }
        }
    })
}

//Delete post by ID
const deleteById = (req, res) => {
    pool.query(`delete from reddit_data where id=${req.params.id}`, (err, data) => {
        if (err) {
            console.error(err)
            res.json({ error: err })
        } else {
            if(data.rowCount === 0) {
                res.json({error: 'Post not fount'})
            } else {
                res.json({message: 'Post deleted'})
            }
        }
    })
}

module.exports = {
    getSeries,
    createNewPost,
    getById,
    updateById,
    deleteById
}