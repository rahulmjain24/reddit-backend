const dotenv = require('dotenv')
dotenv.config()

module.exports = {
    port: process.env.PORT,
    user: process.env.USER,
    password: process.env.PASSWORD,
    database: process.env.DATABASE,
    host: process.env.HOST
}